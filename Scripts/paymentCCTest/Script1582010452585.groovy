import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo.midtrans.com/')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_BUY NOW'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Page_Sample Store/div_CHECKOUT'))

WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Continue'))

WebUI.click(findTestObject('Object Repository/Page_Sample Store/div_Credit Card'))

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_sample-store-1582014767_cardnumber'), '4811 1111 1111 1114')

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input'), '02 / 20')

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_1'), '1')

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_2'), '12')

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_3'), '123')

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_4'), '123')

WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Pay Now'))

WebUI.delay(10)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Sample Store/h1_Issuing Bank'), 0)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Sample Store/input_Password_PaRes'), '4tAN/DuJV7Y=')

WebUI.click(findTestObject('Object Repository/Page_Sample Store/button_OK'))

WebUI.click(findTestObject('Object Repository/Page_Sample Store/span_Thank you for your purchase'))

WebUI.closeBrowser()

