import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo.midtrans.com/')

WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_BUY NOW'))

WebUI.click(findTestObject('Object Repository/Page_Sample Store/div_CHECKOUT'))

WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Continue (2)'))

WebUI.click(findTestObject('Object Repository/Page_Sample Store/div_Credit Card (2)'))

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_sample-store-1582021310_cardnumber'), '4911 1111 1111 1113')

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input (2)'), '02 / 20')

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_1 (2)'), '1')

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_2 (2)'), '12')

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_3 (2)'), '123')

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_4 (2)'), '123')

WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Pay Now (2)'))

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Sample Store/input_Password_PaRes (1)'), '4tAN/DuJV7Y=')

WebUI.click(findTestObject('Object Repository/Page_Sample Store/button_OK (1)'))

WebUI.click(findTestObject('Object Repository/Page_Sample Store/span_Transaction failed'))

WebUI.click(findTestObject('Object Repository/Page_Sample Store/span_Your card got declined by the bank'))

WebUI.closeBrowser()

