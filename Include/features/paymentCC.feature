#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@PaymentCC
Feature: Credit Card payment feature
  As a user I want to pay my order with credit card so that I can finish my transaction

  @Valid
  Scenario Outline: Payment success with a valid credit card number
    Given I want to pay the transaction with credit card
    When I enter a credit card number <credit_card_number>, expiry date <expiry_date>, and cvv <cvv>
    And I click the pay now button
    And I navigate to OTP page
    And I enter the OTP number
    And I click the OK button
    Then I get a message transaction successfull

    Examples: 
      | credit_card_number  | expiry_date | cvv |
      | 4811 1111 1111 1114 | 02/20       | 123 |

  @Invalid
  Scenario Outline: Payment failed with an invalid credit card number
    Given I want to pay the transaction with credit card
    When I enter a credit card number <credit_card_number>, expiry date <expiry_date>, and cvv <cvv>
    And I click the pay now button
    And I navigate to OTP page
    And I enter the OTP number
    And I click the OK button
    Then I get a message payment rejected

    Examples: 
      | credit_card_number  | expiry_date | cvv |
      | 4911 1111 1111 1113 | 02/20       | 123 |
