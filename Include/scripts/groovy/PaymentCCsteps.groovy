import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class PaymentCCsteps {

	@Given("I want to pay the transaction with credit card")
	def payWithCreditCard(){
		println ("\n I am inside Credit Card payment page")
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://demo.midtrans.com/')
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_BUY NOW'))
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/div_CHECKOUT'))
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Continue'))
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/div_Credit Card'))
	}

	@When("I enter a credit card number (.*), expiry date (.*), and cvv (.*)")
	def enterCreditCardInfo(String credit_card_number, String expiry_date, String cvv){
		println ("\n I am inside enter CC info")
		println ("Credit Card Number : "+credit_card_number)
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_sample-store-1582014767_cardnumber'), credit_card_number)
		println ("Expiry date : "+expiry_date)
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input'), expiry_date)
		println ("CVV : "+cvv)
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_3'), cvv)
	}

	@And("I click the pay now button")
	def clickPayNow(){
		println ("I am clicking pay now button")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Pay Now'))
	}

	@And("I navigate to OTP page")
	def verifyOTPPage(){
		println ("I am inside OTP pages")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Sample Store/h1_Issuing Bank'), 0)
	}

	@And("I enter the OTP number")
	def enterOTPNumber(){
		println ("I am inside OTP pages and entering the OTP number")
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_Sample Store/input_Password_PaRes'), '4tAN/DuJV7Y=')
	}

	@And("I click the OK button")
	def clickOK(){
		println ("I am inside OTP page and click OK button")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/button_OK'))
	}

	@Then("I get a message transaction successfull")
	def transactionSuccess(){
		println ("Transaction success")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/span_Thank you for your purchase'))
		WebUI.closeBrowser()
	}

	@Then("I get a message payment rejected")
	def transactionFailed(){
		println ("Transaction failed")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/span_Transaction failed'))
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/span_Your card got declined by the bank'))
		WebUI.closeBrowser()
	}
}