$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/ASUS/git/midtrans-demo/Include/features/paymentCC.feature");
formatter.feature({
  "name": "Credit Card payment feature",
  "description": "  As a user I want to pay my order with credit card so that I can finish my transaction",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@PaymentCC"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Payment success with a valid credit card number",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Valid"
    }
  ]
});
formatter.step({
  "name": "I want to pay the transaction with credit card",
  "keyword": "Given "
});
formatter.step({
  "name": "I enter a credit card number \u003ccredit_card_number\u003e, expiry date \u003cexpiry_date\u003e, and cvv \u003ccvv\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "I click the pay now button",
  "keyword": "And "
});
formatter.step({
  "name": "I navigate to OTP page",
  "keyword": "And "
});
formatter.step({
  "name": "I enter the OTP number",
  "keyword": "And "
});
formatter.step({
  "name": "I click the OK button",
  "keyword": "And "
});
formatter.step({
  "name": "I get a message transaction successfull",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "credit_card_number",
        "expiry_date",
        "cvv"
      ]
    },
    {
      "cells": [
        "4811 1111 1111 1114",
        "02/20",
        "123"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Payment success with a valid credit card number",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@PaymentCC"
    },
    {
      "name": "@Valid"
    }
  ]
});
formatter.step({
  "name": "I want to pay the transaction with credit card",
  "keyword": "Given "
});
formatter.match({
  "location": "PaymentCCsteps.payWithCreditCard()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter a credit card number 4811 1111 1111 1114, expiry date 02/20, and cvv 123",
  "keyword": "When "
});
formatter.match({
  "location": "PaymentCCsteps.enterCreditCardInfo(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click the pay now button",
  "keyword": "And "
});
formatter.match({
  "location": "PaymentCCsteps.clickPayNow()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I navigate to OTP page",
  "keyword": "And "
});
formatter.match({
  "location": "PaymentCCsteps.verifyOTPPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the OTP number",
  "keyword": "And "
});
formatter.match({
  "location": "PaymentCCsteps.enterOTPNumber()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click the OK button",
  "keyword": "And "
});
formatter.match({
  "location": "PaymentCCsteps.clickOK()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I get a message transaction successfull",
  "keyword": "Then "
});
formatter.match({
  "location": "PaymentCCsteps.transactionSuccess()"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Payment failed with an invalid credit card number",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Invalid"
    }
  ]
});
formatter.step({
  "name": "I want to pay the transaction with credit card",
  "keyword": "Given "
});
formatter.step({
  "name": "I enter a credit card number \u003ccredit_card_number\u003e, expiry date \u003cexpiry_date\u003e, and cvv \u003ccvv\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "I click the pay now button",
  "keyword": "And "
});
formatter.step({
  "name": "I navigate to OTP page",
  "keyword": "And "
});
formatter.step({
  "name": "I enter the OTP number",
  "keyword": "And "
});
formatter.step({
  "name": "I click the OK button",
  "keyword": "And "
});
formatter.step({
  "name": "I get a message payment rejected",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "credit_card_number",
        "expiry_date",
        "cvv"
      ]
    },
    {
      "cells": [
        "4911 1111 1111 1113",
        "02/20",
        "123"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Payment failed with an invalid credit card number",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@PaymentCC"
    },
    {
      "name": "@Invalid"
    }
  ]
});
formatter.step({
  "name": "I want to pay the transaction with credit card",
  "keyword": "Given "
});
formatter.match({
  "location": "PaymentCCsteps.payWithCreditCard()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter a credit card number 4911 1111 1111 1113, expiry date 02/20, and cvv 123",
  "keyword": "When "
});
formatter.match({
  "location": "PaymentCCsteps.enterCreditCardInfo(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click the pay now button",
  "keyword": "And "
});
formatter.match({
  "location": "PaymentCCsteps.clickPayNow()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I navigate to OTP page",
  "keyword": "And "
});
formatter.match({
  "location": "PaymentCCsteps.verifyOTPPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the OTP number",
  "keyword": "And "
});
formatter.match({
  "location": "PaymentCCsteps.enterOTPNumber()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click the OK button",
  "keyword": "And "
});
formatter.match({
  "location": "PaymentCCsteps.clickOK()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I get a message payment rejected",
  "keyword": "Then "
});
formatter.match({
  "location": "PaymentCCsteps.transactionFailed()"
});
formatter.result({
  "status": "passed"
});
});