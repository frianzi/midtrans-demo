# midtrans-demo

I created this project using Katalon Studio version 6.2.2
Supported google chrome version is chrome 74

To run the test, follow the instruction below :
1. Clone this project
2. Copy project path (will use it later)
3. Go to katalon studio directory
4. Run this command :
```katalon -noSplash  -runMode=console -consoleLog -noExit -projectPath="{{paste your project path here}}" -retry=0 -testSuitePath="Test Suites/paymentCC" -executionProfile="default" -browserType="Chrome"```
5. Make sure you have replace project path above